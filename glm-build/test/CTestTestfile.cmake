# CMake generated Testfile for 
# Source directory: /home/popsulfr/work/vulkan-playground/glm/test
# Build directory: /home/popsulfr/work/vulkan-playground/glm-build/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(bug)
subdirs(core)
subdirs(gtc)
subdirs(gtx)
