/* easyloggingpp section BEGIN */
#include <easylogging++.h>

INITIALIZE_EASYLOGGINGPP

/* easyloggingpp section END */

/* NUKE Logging BEGIN */
#define NUKE_CLOG_TRACE(id) CLOG(TRACE,id)
#define NUKE_CLOG_DEBUG(id) CLOG(DEBUG,id)
#define NUKE_CLOG_FATAL(id) CLOG(FATAL,id)
#define NUKE_CLOG_ERROR(id) CLOG(ERROR,id)
#define NUKE_CLOG_WARNING(id) CLOG(WARNING,id)
#define NUKE_CLOG_INFO(id) CLOG(INFO,id)
#define NUKE_CLOG_VERBOSE(id) CVLOG(1,id)

#define NUKE_CLOG(LEVEL,id) NUKE_CLOG_##LEVEL(id)

#define NUKE_LOG_TRACE LOG(TRACE)
#define NUKE_LOG_DEBUG LOG(DEBUG)
#define NUKE_LOG_FATAL LOG(FATAL)
#define NUKE_LOG_ERROR LOG(ERROR)
#define NUKE_LOG_WARNING LOG(WARNING)
#define NUKE_LOG_INFO LOG(INFO)
#define NUKE_LOG_VERBOSE VLOG(1)

#define NUKE_LOG(LEVEL) NUKE_LOG_##LEVEL
/* NUKE Logging END */

/* SDL section BEGIN */
#include <SDL2/SDL.h>

#include <string>
#include <exception>

namespace nuke {
class SDLException : public std::exception {
public:
	SDLException(const std::string& msg=::SDL_GetError()):std::exception(),_msg(msg) {}
	virtual ~SDLException() throw() {}
	virtual const char* what() const throw() {
		return this->_msg.c_str();
	}
private:
	std::string _msg;
};

class SDL {
public:
	static constexpr const char * SDL_LOGGER_ID = "sdl";
	SDL() {
		el::Loggers::getLogger(SDL_LOGGER_ID,true);
		::SDL_LogSetOutputFunction([](void* userdata,
										int category, 
										::SDL_LogPriority priority,
										const char * message
 									) {
			(void)userdata;
			(void)category;
			switch (priority) {
				case ::SDL_LOG_PRIORITY_CRITICAL:
					NUKE_CLOG(FATAL,SDL_LOGGER_ID) << message;
					break;
				case ::SDL_LOG_PRIORITY_ERROR:
					NUKE_CLOG(ERROR,SDL_LOGGER_ID) << message;
					break;
				case ::SDL_LOG_PRIORITY_WARN:
					NUKE_CLOG(WARNING,SDL_LOGGER_ID) << message;
					break;
				case ::SDL_LOG_PRIORITY_INFO:
					NUKE_CLOG(INFO,SDL_LOGGER_ID) << message;
					break;
				case ::SDL_LOG_PRIORITY_DEBUG:
					NUKE_CLOG(DEBUG,SDL_LOGGER_ID) << message;
					break;
				case ::SDL_LOG_PRIORITY_VERBOSE:
					NUKE_CLOG(VERBOSE,SDL_LOGGER_ID) << message;
					break;
				default:
					NUKE_CLOG(INFO,SDL_LOGGER_ID) << message;
			}
		},nullptr);
		::SDL_version version;
		SDL_VERSION(&version);
		NUKE_CLOG(INFO,SDL_LOGGER_ID) << "Compiled against SDL " 
		<< std::to_string(version.major) << "." << std::to_string(version.minor) << "." << std::to_string(version.patch);
		SDL_GetVersion(&version);
		NUKE_CLOG(INFO,SDL_LOGGER_ID) << "Linked against SDL " 
		<< std::to_string(version.major) << "." << std::to_string(version.minor) << "." << std::to_string(version.patch);
		if (::SDL_Init(SDL_INIT_EVENTS|SDL_INIT_GAMECONTROLLER|SDL_INIT_HAPTIC) != 0) throw SDLException();
	}
	
	virtual ~SDL() {
		::SDL_Quit();
	}
private:
};
}
/* SDL section END */

/* NUKE Messagebox BEGIN */
namespace nuke {
class Messagebox {
public:
	typedef enum {
		MESSAGEBOX_ERROR=SDL_MESSAGEBOX_ERROR,
		MESSAGEBOX_WARNING=SDL_MESSAGEBOX_WARNING,
		MESSAGEBOX_INFO=SDL_MESSAGEBOX_INFORMATION
	} MESSAGEBOXTYPE;
	static bool showMessageBox(const char * title, const char* message,MESSAGEBOXTYPE mbtype=MESSAGEBOX_INFO) {
		return ::SDL_ShowSimpleMessageBox(mbtype,title,message,nullptr);
	}
private:
	Messagebox() {};
};
}
/* NUKE Messagebox END */

/* GLFW section BEGIN */
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

namespace nuke {
class GLFWException:public std::exception {
public:
	GLFWException():std::exception() {}
	virtual ~GLFWException() throw() {}
	virtual const char* what() const throw() {
		return "GLFW Error";
	}
};
class GLFW {
public:
	static constexpr const char * GLFW_LOGGER_ID = "glfw";
	GLFW() {
		el::Loggers::getLogger(GLFW_LOGGER_ID,true);
		::glfwSetErrorCallback([](int error,const char* message) {
			NUKE_CLOG(ERROR,GLFW_LOGGER_ID) << "ERROR " << error << " : " << message;
		});
		NUKE_CLOG(INFO,nuke::GLFW::GLFW_LOGGER_ID) << ::glfwGetVersionString();
		NUKE_CLOG(INFO,GLFW_LOGGER_ID) << "Compiled against GLFW " 
		<< std::to_string(GLFW_VERSION_MAJOR) << "." << std::to_string(GLFW_VERSION_MINOR) << "." << std::to_string(GLFW_VERSION_REVISION);
		int major, minor, revision;
		::glfwGetVersion(&major,&minor,&revision);
		NUKE_CLOG(INFO,GLFW_LOGGER_ID) << "Linked against GLFW " 
		<< std::to_string(major) << "." << std::to_string(minor) << "." << std::to_string(revision);
		if (::glfwInit() == GLFW_FALSE) throw GLFWException();
	}
	
	virtual ~GLFW() {
		::glfwTerminate();
	}
};
}
/* GLFW section END */

/* NUKE Renderer BEGIN */

#include <vector>
#include <memory>
#include <cstring>
#include <functional>
#include <iostream>

#define NUKE_RENDERER_EXCEPTION(msg) {NUKE_CLOG(FATAL,RENDERER_LOGGER_ID) << msg; throw RendererException(msg);}
#define FENCE_TIMEOUT 100000000

namespace nuke {
class RendererException:public std::exception {
public:
	RendererException(const std::string &msg):_msg(msg) {}
	virtual ~RendererException() throw() {}
	virtual const char* what() const throw() {
		return this->_msg.c_str();
	}
protected:
	std::string _msg;
};
class Renderer {
public:
	static constexpr const char * RENDERER_LOGGER_ID = "renderer";
	
	static constexpr int WINDOW_WIDTH = 800;
	static constexpr int WINDOW_HEIGHT = 600;
	
	struct {
	public:
		/* Init */
		PFN_vkEnumerateInstanceLayerProperties EnumerateInstanceLayerProperties = nullptr;
		PFN_vkEnumerateInstanceExtensionProperties EnumerateInstanceExtensionProperties = nullptr;
		PFN_vkCreateInstance CreateInstance = nullptr;
		PFN_vkDestroyInstance DestroyInstance = nullptr;
		/* Device */
		PFN_vkEnumeratePhysicalDevices EnumeratePhysicalDevices = nullptr;
		PFN_vkGetPhysicalDeviceProperties GetPhysicalDeviceProperties = nullptr;
		PFN_vkGetPhysicalDeviceMemoryProperties GetPhysicalDeviceMemoryProperties = nullptr;
		PFN_vkCreateDevice CreateDevice = nullptr;
		PFN_vkDestroyDevice DestroyDevice = nullptr;
		PFN_vkGetPhysicalDeviceQueueFamilyProperties GetPhysicalDeviceQueueFamilyProperties = nullptr;
		PFN_vkGetDeviceProcAddr GetDeviceProcAddr = nullptr;
		/* Surface */
		PFN_vkDestroySurfaceKHR DestroySurfaceKHR = nullptr;
		/* Command Pool */
		PFN_vkCreateCommandPool CreateCommandPool = nullptr;
		PFN_vkDestroyCommandPool DestroyCommandPool = nullptr;
		/* Command Buffers */
		PFN_vkAllocateCommandBuffers AllocateCommandBuffers = nullptr;
		PFN_vkFreeCommandBuffers FreeCommandBuffers = nullptr;
		PFN_vkBeginCommandBuffer BeginCommandBuffer = nullptr;
		PFN_vkEndCommandBuffer EndCommandBuffer = nullptr;
		PFN_vkCmdPipelineBarrier CmdPipelineBarrier = nullptr;
		/* Swapchain */
		PFN_vkGetPhysicalDeviceSurfaceFormatsKHR GetPhysicalDeviceSurfaceFormatsKHR = nullptr;
		PFN_vkGetPhysicalDeviceSurfaceCapabilitiesKHR GetPhysicalDeviceSurfaceCapabilitiesKHR = nullptr;
		PFN_vkGetPhysicalDeviceSurfacePresentModesKHR GetPhysicalDeviceSurfacePresentModesKHR = nullptr;
		PFN_vkCreateSwapchainKHR CreateSwapchainKHR = nullptr;
		PFN_vkGetSwapchainImagesKHR GetSwapchainImagesKHR = nullptr;
		PFN_vkDestroySwapchainKHR DestroySwapchainKHR = nullptr;
		PFN_vkCreateImageView CreateImageView = nullptr;
		PFN_vkDestroyImageView DestroyImageView = nullptr;
		/* Queue */
		PFN_vkGetDeviceQueue GetDeviceQueue = nullptr;
		PFN_vkCreateFence CreateFence = nullptr;
		PFN_vkQueueSubmit QueueSubmit = nullptr;
		PFN_vkWaitForFences WaitForFences = nullptr;
		PFN_vkDestroyFence DestroyFence = nullptr;
		/* Depth Buffer */
		PFN_vkGetPhysicalDeviceFormatProperties GetPhysicalDeviceFormatProperties = nullptr;
		PFN_vkCreateImage CreateImage = nullptr;
		PFN_vkDestroyImage DestroyImage = nullptr;
		PFN_vkGetImageMemoryRequirements GetImageMemoryRequirements = nullptr;
		PFN_vkAllocateMemory AllocateMemory = nullptr;
		PFN_vkFreeMemory FreeMemory = nullptr;
		PFN_vkBindImageMemory BindImageMemory = nullptr;
	} VK;
	
	typedef struct {
		VkImage image;
		VkImageView view;
	} VkSwapChainBuffer;
	
	typedef struct {
		VkLayerProperties properties;
		std::vector<VkExtensionProperties> extensions;
	} R_VkLayerProperties;
	
	Renderer():_glfw(), _vkswapchainBuffers(0) {
		el::Loggers::getLogger(RENDERER_LOGGER_ID,true);
		if (!::glfwVulkanSupported()) NUKE_RENDERER_EXCEPTION("Vulkan not available")
		NUKE_CLOG(INFO,RENDERER_LOGGER_ID) << "Vulkan Support : true";
		uint32_t count;
		const char** extensions = ::glfwGetRequiredInstanceExtensions(&count);
		if (!count || !extensions) NUKE_RENDERER_EXCEPTION("No instance extensions")
		std::string ext_strings;
		for (uint32_t i = 0; i < count; ++i) {
			if (i) ext_strings += ", ";
			ext_strings += extensions[i];
		}
		NUKE_CLOG(INFO,RENDERER_LOGGER_ID) << "Vulkan Required Instance Extensions " << std::to_string(count) << " : " << ext_strings;
		
		this->_initInstance();
		this->_initDevice();
		this->_initWindow();
		this->_initCommandPool();
		this->_initCommandBuffer();
		this->_executeBeginCommandBuffer();
		this->_initQueue();
		this->_initSwapchain();
	}
	virtual ~Renderer() {
		this->_deinitSwapchain();
		this->_deinitCommandBuffer();
		this->_deinitCommandPool();
		this->_deinitWindow();
		this->_deinitDevice();
		this->_deinitInstance();
	}
	
	virtual void mainLoop () {
		while (!::glfwWindowShouldClose(this->_glfwWindow)) {
			// Keep running
			::glfwPollEvents();
		}
	}
protected:
	virtual void _initInstance() {
		if (!(VK.EnumerateInstanceLayerProperties = (PFN_vkEnumerateInstanceLayerProperties)::glfwGetInstanceProcAddress(nullptr,"vkEnumerateInstanceLayerProperties")))
			NUKE_RENDERER_EXCEPTION("No address for vkEnumerateInstanceLayerProperties")
		if (!(VK.EnumerateInstanceExtensionProperties = (PFN_vkEnumerateInstanceExtensionProperties)::glfwGetInstanceProcAddress(nullptr,"vkEnumerateInstanceExtensionProperties")))
			NUKE_RENDERER_EXCEPTION("No address for vkEnumerateInstanceExtensionProperties")
		if (!(VK.CreateInstance = (PFN_vkCreateInstance)::glfwGetInstanceProcAddress(nullptr,"vkCreateInstance")))
			NUKE_RENDERER_EXCEPTION("No address for vkCreateInstance")
		if (!(VK.DestroyInstance = (PFN_vkDestroyInstance)::glfwGetInstanceProcAddress(nullptr,"vkDestroyInstance")))
			NUKE_RENDERER_EXCEPTION("No address for vkDestroyInstance")
			
		{
			/*
			* It's possible, though very rare, that the number of
			* instance layers could change. For example, installing something
			* could include new layers that the loader would pick up
			* between the initial query for the count and the
			* request for VkLayerProperties. The loader indicates that
			* by returning a VK_INCOMPLETE status and will update the
			* the count parameter.
			* The count parameter will be updated with the number of
			* entries loaded into the data pointer - in case the number
			* of layers went down or is smaller than the size given.
			*/
			uint32_t instanceLayerCount = 0;
			std::vector<VkLayerProperties> vklpList(0);
			for (::VkResult res = VK_INCOMPLETE; res == VK_INCOMPLETE;) {
				if (VK.EnumerateInstanceLayerProperties(&instanceLayerCount, nullptr) != VK_SUCCESS
					|| instanceLayerCount == 0) {
					instanceLayerCount = 0;
					break;
				}
				vklpList.resize(instanceLayerCount);
				res = VK.EnumerateInstanceLayerProperties(&instanceLayerCount, vklpList.data());
			}
			if (instanceLayerCount > 0) {
				for (VkLayerProperties vklp:vklpList) {
					R_VkLayerProperties rvklp;
					rvklp.properties = vklp;
					uint32_t instanceExtensionCount = 0;
					for (::VkResult res = VK_INCOMPLETE; res == VK_INCOMPLETE;) {
						if (VK.EnumerateInstanceExtensionProperties(vklp.layerName, &instanceExtensionCount, nullptr) != VK_SUCCESS
							|| instanceExtensionCount == 0) {
							instanceExtensionCount = 0;
							break;
						}
						rvklp.extensions.resize(instanceExtensionCount);
						res = VK.EnumerateInstanceExtensionProperties(vklp.layerName, &instanceExtensionCount, rvklp.extensions.data());
					}
					if (instanceExtensionCount > 0) {
						this->_vklayerProperties.push_back(rvklp);
					}
				}
			}
		}
		
		{
			// Need to be careful that there are no duplicates in the list
			bool has_VK_KHR_SURFACE_EXTENSION_NAME = false;
			uint32_t requiredExtensionCount = 0;
			const char ** requiredExtensions = ::glfwGetRequiredInstanceExtensions(&requiredExtensionCount);
			for (uint32_t i = 0; i < requiredExtensionCount; ++i) {
				if (!strcmp(requiredExtensions[i], VK_KHR_SURFACE_EXTENSION_NAME)) {
					has_VK_KHR_SURFACE_EXTENSION_NAME = true;
				}
				this->_vkinstanceExtensionNames.push_back(requiredExtensions[i]);
			}
			if (!has_VK_KHR_SURFACE_EXTENSION_NAME) {
				this->_vkinstanceExtensionNames.push_back(VK_KHR_SURFACE_EXTENSION_NAME);
			}
		}
		
		::VkApplicationInfo vkai = {};
		vkai.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
		vkai.apiVersion = VK_MAKE_VERSION(1,0,2);
		vkai.applicationVersion = VK_MAKE_VERSION(0,0,1);
		vkai.pApplicationName = "Nuke";
		vkai.engineVersion = VK_MAKE_VERSION(0,0,1);
		vkai.pEngineName = "Nuke";
		
		::VkInstanceCreateInfo vkici = {};
		vkici.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		vkici.pApplicationInfo = &vkai;
		vkici.enabledExtensionCount = this->_vkinstanceExtensionNames.size();
		vkici.ppEnabledExtensionNames = (this->_vkinstanceExtensionNames.size()) ? this->_vkinstanceExtensionNames.data() : nullptr;
		vkici.enabledLayerCount = this->_vkinstanceLayerNames.size();
		vkici.ppEnabledLayerNames = (this->_vkinstanceLayerNames.size()) ? this->_vkinstanceLayerNames.data() : nullptr;
		
		if (VK.CreateInstance(&vkici,nullptr,&this->_vkinstance) != VK_SUCCESS)
			NUKE_RENDERER_EXCEPTION("vkCreateInstance failure")
	}
	
	virtual void _deinitInstance() {
		if (this->_vkinstance && VK.DestroyInstance)
			VK.DestroyInstance(this->_vkinstance,nullptr);
	}
	
	virtual void _initDevice () {
		if (!(VK.EnumeratePhysicalDevices = (PFN_vkEnumeratePhysicalDevices)::glfwGetInstanceProcAddress(this->_vkinstance,"vkEnumeratePhysicalDevices")))
			NUKE_RENDERER_EXCEPTION("No address for vkEnumeratePhysicalDevices")
		if (!(VK.GetPhysicalDeviceProperties = (PFN_vkGetPhysicalDeviceProperties)::glfwGetInstanceProcAddress(this->_vkinstance,"vkGetPhysicalDeviceProperties")))
			NUKE_RENDERER_EXCEPTION("No address for vkGetPhysicalDeviceProperties")
		if (!(VK.GetPhysicalDeviceMemoryProperties = (PFN_vkGetPhysicalDeviceMemoryProperties)::glfwGetInstanceProcAddress(this->_vkinstance,"vkGetPhysicalDeviceMemoryProperties")))
			NUKE_RENDERER_EXCEPTION("No address for vkGetPhysicalDeviceMemoryProperties")
		if (!(VK.CreateDevice = (PFN_vkCreateDevice)::glfwGetInstanceProcAddress(this->_vkinstance,"vkCreateDevice")))
			NUKE_RENDERER_EXCEPTION("No address for vkCreateDevice")
		if (!(VK.DestroyDevice = (PFN_vkDestroyDevice)::glfwGetInstanceProcAddress(this->_vkinstance,"vkDestroyDevice")))
			NUKE_RENDERER_EXCEPTION("No address for vkDestroyDevice")
		if (!(VK.GetPhysicalDeviceQueueFamilyProperties = (PFN_vkGetPhysicalDeviceQueueFamilyProperties)::glfwGetInstanceProcAddress(this->_vkinstance,"vkGetPhysicalDeviceQueueFamilyProperties")))
			NUKE_RENDERER_EXCEPTION("No address for vkGetPhysicalDeviceQueueFamilyProperties")
		
		{
			uint32_t gpuCount = 0;
			if (VK.EnumeratePhysicalDevices(this->_vkinstance,&gpuCount,nullptr) != VK_SUCCESS)
				NUKE_RENDERER_EXCEPTION("vkEnumeratePhysicalDevices failure")
			std::vector<VkPhysicalDevice> gpuList(gpuCount);
			if (VK.EnumeratePhysicalDevices(this->_vkinstance,&gpuCount,gpuList.data()) != VK_SUCCESS)
				NUKE_RENDERER_EXCEPTION("vkEnumeratePhysicalDevices failure")
			
			this->_vkgpu = gpuList[0];
			
			::VkPhysicalDeviceProperties vkpdp_one;
			::VkPhysicalDeviceMemoryProperties vkpdmp_one;
			VK.GetPhysicalDeviceProperties(this->_vkgpu,&vkpdp_one);
			VK.GetPhysicalDeviceMemoryProperties(this->_vkgpu,&vkpdmp_one);
			
			for (VkPhysicalDevice vkpd:gpuList) {
				if (this->_vkgpu == vkpd) continue;
				::VkPhysicalDeviceProperties vkpdp;
				::VkPhysicalDeviceMemoryProperties vkpdmp;
				VK.GetPhysicalDeviceProperties(vkpd,&vkpdp);
				VK.GetPhysicalDeviceMemoryProperties(vkpd,&vkpdmp);
				if (vkpdp.deviceType != VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) continue;
				if (vkpdp_one.deviceType != VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) {
					this->_vkgpu = vkpd;
					VK.GetPhysicalDeviceProperties(this->_vkgpu,&vkpdp_one);
					VK.GetPhysicalDeviceMemoryProperties(this->_vkgpu,&vkpdmp_one);
					continue;
				}
				else {
					bool vkpdmp_has_local_bit = false;
					for (uint32_t i = 0; i < vkpdmp.memoryTypeCount; ++i) {
						if (vkpdmp.memoryTypes[i].propertyFlags & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) {
							vkpdmp_has_local_bit = true;
							break;
						}
					}
					if (!vkpdmp_has_local_bit) continue;
					bool vkpdmp_one_has_local_bit = false;
					for (uint32_t i = 0; i < vkpdmp_one.memoryTypeCount; ++i) {
						if (vkpdmp_one.memoryTypes[i].propertyFlags & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) {
							vkpdmp_one_has_local_bit = true;
							break;
						}
					}
					if (!vkpdmp_one_has_local_bit) {
						this->_vkgpu = vkpd;
						VK.GetPhysicalDeviceProperties(this->_vkgpu,&vkpdp_one);
						VK.GetPhysicalDeviceMemoryProperties(this->_vkgpu,&vkpdmp_one);
						continue;
					}
					uint64_t vkpdmp_score = 0;
					for (uint32_t i = 0; i < vkpdmp.memoryHeapCount; ++i) {
						if (vkpdmp.memoryHeaps[i].flags & VK_MEMORY_HEAP_DEVICE_LOCAL_BIT) {
							vkpdmp_score += vkpdmp.memoryHeaps[i].size <<1;
						}
						else {
							vkpdmp_score += vkpdmp.memoryHeaps[i].size;
						}
					}
					uint64_t vkpdmp_one_score = 0;
					for (uint32_t i = 0; i < vkpdmp_one.memoryHeapCount; ++i) {
						if (vkpdmp_one.memoryHeaps[i].flags & VK_MEMORY_HEAP_DEVICE_LOCAL_BIT) {
							vkpdmp_one_score += vkpdmp_one.memoryHeaps[i].size <<1;
						}
						else {
							vkpdmp_one_score += vkpdmp_one.memoryHeaps[i].size;
						}
					}
					if (vkpdmp_score > vkpdmp_one_score) {
						this->_vkgpu = vkpd;
						VK.GetPhysicalDeviceProperties(this->_vkgpu,&vkpdp_one);
						VK.GetPhysicalDeviceMemoryProperties(this->_vkgpu,&vkpdmp_one);
						continue;
					}
				}
			}
			
			NUKE_CLOG(INFO,RENDERER_LOGGER_ID) << "Using GPU : " << vkpdp_one.deviceName;
		}
		
		{
			uint32_t familyCount = 0;
			VK.GetPhysicalDeviceQueueFamilyProperties(this->_vkgpu,&familyCount,nullptr);
			std::vector<VkQueueFamilyProperties> familyPropertyList(familyCount);
			VK.GetPhysicalDeviceQueueFamilyProperties(this->_vkgpu,&familyCount,familyPropertyList.data());
			bool found = false;
			for (uint32_t i = 0; i < familyCount; ++i) {
				if (::glfwGetPhysicalDevicePresentationSupport(this->_vkinstance,this->_vkgpu,i) == GLFW_TRUE
					&& familyPropertyList[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
					found = true;
					this->_vkgraphicsFamilyIndex = i;
					break;
				}
			}
			if (!found) NUKE_RENDERER_EXCEPTION("Queue family supporting graphics not found")
		}
		
		{
			//  We want at least the swapchain extension
			bool has_VK_KHR_SWAPCHAIN_EXTENSION_NAME = false;
			for (const char * name:this->_vkdeviceExtensionNames) {
				if (!strcmp(name, VK_KHR_SWAPCHAIN_EXTENSION_NAME)) {
					has_VK_KHR_SWAPCHAIN_EXTENSION_NAME = true;
					break;
				}
			}
			if (!has_VK_KHR_SWAPCHAIN_EXTENSION_NAME)
				this->_vkdeviceExtensionNames.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);
		}
		
		float queuePriorities[] = {1.0f};
		
		::VkDeviceQueueCreateInfo vkdqci = {};
		vkdqci.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		vkdqci.queueFamilyIndex = this->_vkgraphicsFamilyIndex;
		vkdqci.queueCount = 1;
		vkdqci.pQueuePriorities = queuePriorities;
		
		::VkDeviceCreateInfo vkdci = {};
		vkdci.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
		vkdci.queueCreateInfoCount = 1;
		vkdci.pQueueCreateInfos = &vkdqci;
		vkdci.enabledExtensionCount = this->_vkdeviceExtensionNames.size();
		vkdci.ppEnabledExtensionNames = (this->_vkdeviceExtensionNames.size()) ? this->_vkdeviceExtensionNames.data() : nullptr;
		vkdci.enabledLayerCount = this->_vkdeviceLayerNames.size();
		vkdci.ppEnabledLayerNames = (this->_vkdeviceLayerNames.size()) ? this->_vkdeviceLayerNames.data() : nullptr;
		
		if (VK.CreateDevice(this->_vkgpu,&vkdci,nullptr,&this->_vkdevice) != VK_SUCCESS)
			NUKE_RENDERER_EXCEPTION("vkCreateDevice failure")
		if (!(VK.GetDeviceProcAddr = (PFN_vkGetDeviceProcAddr)::glfwGetInstanceProcAddress(this->_vkinstance,"vkGetDeviceProcAddr")))
			NUKE_RENDERER_EXCEPTION("No address for vkGetDeviceProcAddr")
	}
	
	virtual void _deinitDevice() {
		if (this->_vkdevice && VK.DestroyDevice)
			VK.DestroyDevice(this->_vkdevice,nullptr);
	}
	
	virtual void _initWindow() {
		if (!(VK.DestroySurfaceKHR = (PFN_vkDestroySurfaceKHR)::glfwGetInstanceProcAddress(this->_vkinstance,"vkDestroySurfaceKHR")))
			NUKE_RENDERER_EXCEPTION("No address for vkDestroySurfaceKHR")
		//  Prevent OpenGL context creation, we handle Vulkan ourselves
		::glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
		// Platform independent window creation
		if (!(this->_glfwWindow = ::glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "Nuke", nullptr, nullptr)))
			NUKE_RENDERER_EXCEPTION("glfwCreateWindow failure")
		// Platform independent vkCreateXXXSurfaceKHR
		if (::glfwCreateWindowSurface(this->_vkinstance,this->_glfwWindow,nullptr,&this->_vksurface) != VK_SUCCESS)
			NUKE_RENDERER_EXCEPTION("glfwCreateWindowSurface failure")
	}
	
	virtual void _deinitWindow() {
		if (this->_vksurface && VK.DestroySurfaceKHR)
			VK.DestroySurfaceKHR(this->_vkinstance,this->_vksurface,nullptr);
		if (this->_glfwWindow)
			::glfwDestroyWindow(this->_glfwWindow);
	}
	
	virtual void _initCommandPool() {
		if (!(VK.CreateCommandPool = (PFN_vkCreateCommandPool)::glfwGetInstanceProcAddress(this->_vkinstance,"vkCreateCommandPool")))
			NUKE_RENDERER_EXCEPTION("No address for vkCreateCommandPool")
		if (!(VK.DestroyCommandPool = (PFN_vkDestroyCommandPool)::glfwGetInstanceProcAddress(this->_vkinstance,"vkDestroyCommandPool")))
			NUKE_RENDERER_EXCEPTION("No address for vkDestroyCommandPool")
		::VkCommandPoolCreateInfo vkcpci = {};
		vkcpci.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
		vkcpci.queueFamilyIndex = this->_vkgraphicsFamilyIndex;
		vkcpci.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
		if (VK.CreateCommandPool(this->_vkdevice,&vkcpci,nullptr,&this->_vkcommandPool) != VK_SUCCESS)
			NUKE_RENDERER_EXCEPTION("vkCreateCommandPool failure")
	}
	
	virtual void _deinitCommandPool () {
		if (this->_vkcommandPool && VK.DestroyCommandPool)
			VK.DestroyCommandPool(this->_vkdevice,this->_vkcommandPool,nullptr);
	}
	
	virtual void _initCommandBuffer () {
		if (!(VK.AllocateCommandBuffers = (PFN_vkAllocateCommandBuffers)::glfwGetInstanceProcAddress(this->_vkinstance,"vkAllocateCommandBuffers")))
			NUKE_RENDERER_EXCEPTION("No address for vkAllocateCommandBuffers")
		if (!(VK.FreeCommandBuffers = (PFN_vkFreeCommandBuffers)::glfwGetInstanceProcAddress(this->_vkinstance,"vkFreeCommandBuffers")))
			NUKE_RENDERER_EXCEPTION("No address for vkFreeCommandBuffers")
		
		if (!(VK.BeginCommandBuffer = (PFN_vkBeginCommandBuffer)::glfwGetInstanceProcAddress(this->_vkinstance,"vkBeginCommandBuffer")))
			NUKE_RENDERER_EXCEPTION("No address for vkBeginCommandBuffer")
		if (!(VK.EndCommandBuffer = (PFN_vkEndCommandBuffer)::glfwGetInstanceProcAddress(this->_vkinstance,"vkEndCommandBuffer")))
			NUKE_RENDERER_EXCEPTION("No address for vkEndCommandBuffer")
			
		if (!(VK.CmdPipelineBarrier = (PFN_vkCmdPipelineBarrier)::glfwGetInstanceProcAddress(this->_vkinstance,"vkCmdPipelineBarrier")))
			NUKE_RENDERER_EXCEPTION("No address for vkCmdPipelineBarrier")
		
		::VkCommandBufferAllocateInfo vkcbai = {};
		vkcbai.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		vkcbai.commandPool = this->_vkcommandPool;
		vkcbai.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		vkcbai.commandBufferCount = 1;
		
		if (VK.AllocateCommandBuffers(this->_vkdevice,&vkcbai,&this->_vkcommandBuffer) != VK_SUCCESS)
			NUKE_RENDERER_EXCEPTION("vkAllocateCommandBuffers failure")
	}
	
	virtual void _deinitCommandBuffer () {
		if (this->_vkcommandBuffer) {
			if (VK.FreeCommandBuffers) VK.FreeCommandBuffers(this->_vkdevice,this->_vkcommandPool,1,&this->_vkcommandBuffer);
		}
	}
	
	virtual void _executeBeginCommandBuffer () {
		::VkCommandBufferBeginInfo vkcbbi = {};
		vkcbbi.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		
		if (VK.BeginCommandBuffer(this->_vkcommandBuffer,&vkcbbi) != VK_SUCCESS)
			NUKE_RENDERER_EXCEPTION("vkBeginCommandBuffer failure")
	}
	
	virtual void _executeEndCommandBuffer () {
		if (VK.EndCommandBuffer(this->_vkcommandBuffer) != VK_SUCCESS)
			NUKE_RENDERER_EXCEPTION("vkEndCommandBuffer failure")
	}
	
	virtual void _setImageLayout(VkImage image,
						VkImageAspectFlags aspectMask,
						VkImageLayout oldImageLayout,
						VkImageLayout newImageLayout) {
		::VkImageMemoryBarrier imageMemoryBarrier = {};
		imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		imageMemoryBarrier.srcAccessMask = 0;
		imageMemoryBarrier.dstAccessMask = 0;
		imageMemoryBarrier.oldLayout = oldImageLayout;
		imageMemoryBarrier.newLayout = newImageLayout;
		imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		imageMemoryBarrier.image = image;
		imageMemoryBarrier.subresourceRange.aspectMask = aspectMask;
		imageMemoryBarrier.subresourceRange.baseMipLevel = 0;
		imageMemoryBarrier.subresourceRange.levelCount = 1;
		imageMemoryBarrier.subresourceRange.baseArrayLayer = 0;
		imageMemoryBarrier.subresourceRange.layerCount = 1;
		
		if (oldImageLayout == VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL) {
			imageMemoryBarrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		}
		if (newImageLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
			imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		}
		if (newImageLayout == VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL) {
			imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
		}
		if (oldImageLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
			imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		}
		if (oldImageLayout == VK_IMAGE_LAYOUT_PREINITIALIZED) {
			imageMemoryBarrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT;
		}
		if (newImageLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
			imageMemoryBarrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT | VK_ACCESS_TRANSFER_WRITE_BIT;
			imageMemoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
		}
		if (newImageLayout == VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL) {
			imageMemoryBarrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		}
		if (newImageLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
			imageMemoryBarrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
		}
		::VkPipelineStageFlags srcStages = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		::VkPipelineStageFlags destStages = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		VK.CmdPipelineBarrier(this->_vkcommandBuffer, srcStages, destStages, 0, 0, nullptr, 0, nullptr, 1, &imageMemoryBarrier);
	}
	
	virtual void _initQueue() {
		if (!(VK.GetDeviceQueue = (PFN_vkGetDeviceQueue)::glfwGetInstanceProcAddress(this->_vkinstance,"vkGetDeviceQueue")))
			NUKE_RENDERER_EXCEPTION("No address for vkGetDeviceQueue")
		if (!(VK.CreateFence = (PFN_vkCreateFence)::glfwGetInstanceProcAddress(this->_vkinstance,"vkCreateFence")))
			NUKE_RENDERER_EXCEPTION("No address for vkCreateFence")
		if (!(VK.QueueSubmit = (PFN_vkQueueSubmit)::glfwGetInstanceProcAddress(this->_vkinstance,"vkQueueSubmit")))
			NUKE_RENDERER_EXCEPTION("No address for vkQueueSubmit")
		if (!(VK.WaitForFences = (PFN_vkWaitForFences)::glfwGetInstanceProcAddress(this->_vkinstance,"vkWaitForFences")))
			NUKE_RENDERER_EXCEPTION("No address for vkWaitForFences")
		if (!(VK.DestroyFence = (PFN_vkDestroyFence)::glfwGetInstanceProcAddress(this->_vkinstance,"vkDestroyFence")))
			NUKE_RENDERER_EXCEPTION("No address for vkDestroyFence")
			
		VK.GetDeviceQueue(this->_vkdevice, this->_vkgraphicsFamilyIndex, 0, &this->_vkqueue);
	}
	
	virtual void _executeQueueCommandBuffer() {
		::VkFence drawFence = nullptr;
		::VkFenceCreateInfo fenceInfo = {};
		fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
		std::unique_ptr<VkFence,std::function<void(VkFence*)>> 
		drawFence_ptr(&drawFence, [this](VkFence* f) {
			if (f) VK.DestroyFence(this->_vkdevice, *f, nullptr);
		});
		if (VK.CreateFence(this->_vkdevice, &fenceInfo, nullptr, drawFence_ptr.get()) !=  VK_SUCCESS)
			NUKE_RENDERER_EXCEPTION("vkCreateFence failure")
		
		::VkPipelineStageFlags pipeStageFlags = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
		::VkSubmitInfo submitInfo[1] = {};
		submitInfo[0].sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submitInfo[0].waitSemaphoreCount = 0;
		submitInfo[0].pWaitSemaphores = nullptr;
		submitInfo[0].pWaitDstStageMask = &pipeStageFlags;
		submitInfo[0].commandBufferCount = 1;
		submitInfo[0].pCommandBuffers = &this->_vkcommandBuffer;
		submitInfo[0].signalSemaphoreCount = 0;
		submitInfo[0].pSignalSemaphores = nullptr;
		
		if (VK.QueueSubmit(this->_vkqueue, 1, submitInfo, drawFence) !=  VK_SUCCESS)
			NUKE_RENDERER_EXCEPTION("vkQueueSubmit failure")
		
		::VkResult res;
		while ((res = VK.WaitForFences(this->_vkdevice, 1, drawFence_ptr.get(), VK_TRUE, FENCE_TIMEOUT)) ==  VK_TIMEOUT);
		if (res != VK_SUCCESS)
			NUKE_RENDERER_EXCEPTION("vkWaitForFences failure")
	}
	
	virtual void _initSwapchain() {
		if (!(VK.GetPhysicalDeviceSurfaceFormatsKHR = (PFN_vkGetPhysicalDeviceSurfaceFormatsKHR)::glfwGetInstanceProcAddress(this->_vkinstance,"vkGetPhysicalDeviceSurfaceFormatsKHR")))
			NUKE_RENDERER_EXCEPTION("No address for vkGetPhysicalDeviceSurfaceFormatsKHR")
		if (!(VK.GetPhysicalDeviceSurfaceCapabilitiesKHR = (PFN_vkGetPhysicalDeviceSurfaceCapabilitiesKHR)::glfwGetInstanceProcAddress(this->_vkinstance,"vkGetPhysicalDeviceSurfaceCapabilitiesKHR")))
			NUKE_RENDERER_EXCEPTION("No address for vkGetPhysicalDeviceSurfaceCapabilitiesKHR")
		if (!(VK.GetPhysicalDeviceSurfacePresentModesKHR = (PFN_vkGetPhysicalDeviceSurfacePresentModesKHR)::glfwGetInstanceProcAddress(this->_vkinstance,"vkGetPhysicalDeviceSurfacePresentModesKHR")))
			NUKE_RENDERER_EXCEPTION("No address for vkGetPhysicalDeviceSurfacePresentModesKHR")
		if (!(VK.CreateSwapchainKHR = (PFN_vkCreateSwapchainKHR)::glfwGetInstanceProcAddress(this->_vkinstance,"vkCreateSwapchainKHR")))
			NUKE_RENDERER_EXCEPTION("No address for vkCreateSwapchainKHR")
		if (!(VK.GetSwapchainImagesKHR = (PFN_vkGetSwapchainImagesKHR)::glfwGetInstanceProcAddress(this->_vkinstance,"vkGetSwapchainImagesKHR")))
			NUKE_RENDERER_EXCEPTION("No address for vkGetSwapchainImagesKHR")
		if (!(VK.DestroySwapchainKHR = (PFN_vkDestroySwapchainKHR)::glfwGetInstanceProcAddress(this->_vkinstance,"vkDestroySwapchainKHR")))
			NUKE_RENDERER_EXCEPTION("No address for vkDestroySwapchainKHR")
		if (!(VK.CreateImageView = (PFN_vkCreateImageView)::glfwGetInstanceProcAddress(this->_vkinstance,"vkCreateImageView")))
			NUKE_RENDERER_EXCEPTION("No address for vkCreateImageView")
		if (!(VK.DestroyImageView = (PFN_vkDestroyImageView)::glfwGetInstanceProcAddress(this->_vkinstance,"vkDestroyImageView")))
			NUKE_RENDERER_EXCEPTION("No address for vkDestroyImageView")
		{
			uint32_t formatCount = 0;
			if (VK.GetPhysicalDeviceSurfaceFormatsKHR(this->_vkgpu, this->_vksurface, &formatCount, nullptr) !=  VK_SUCCESS)
				NUKE_RENDERER_EXCEPTION("vkGetPhysicalDeviceSurfaceFormatsKHR failure")
			std::vector<VkSurfaceFormatKHR> surfFormatsList(formatCount);
			if (VK.GetPhysicalDeviceSurfaceFormatsKHR(this->_vkgpu, this->_vksurface, &formatCount, surfFormatsList.data()) !=  VK_SUCCESS)
				NUKE_RENDERER_EXCEPTION("vkGetPhysicalDeviceSurfaceFormatsKHR failure")
			// If the format list includes just one entry of VK_FORMAT_UNDEFINED,
			// the surface has no preferred format. So we assume VK_FORMAT_B8G8R8A8_UNORM.
			// Otherwise, at least one supported format will be returned.
			if ((formatCount == 1) && (surfFormatsList[0].format == VK_FORMAT_UNDEFINED)) {
				this->_vkformat = VK_FORMAT_B8G8R8A8_UNORM;
			} else {
				// Always select the first available color format
				// If you need a specific format (e.g. SRGB) you'd need to
				// iterate over the list of available surface format and
				// check for it's presence
				this->_vkformat = surfFormatsList[0].format;
			}
			this->_vkcolorSpace = surfFormatsList[0].colorSpace;
		}
		
		{
			::VkSurfaceCapabilitiesKHR vksck;
			if (VK.GetPhysicalDeviceSurfaceCapabilitiesKHR(this->_vkgpu, this->_vksurface, &vksck) !=  VK_SUCCESS)
				NUKE_RENDERER_EXCEPTION("vkGetPhysicalDeviceSurfaceCapabilitiesKHR failure")
			uint32_t presentModeCount = 0;
			if (VK.GetPhysicalDeviceSurfacePresentModesKHR(this->_vkgpu, this->_vksurface, &presentModeCount, nullptr) != VK_SUCCESS)
				NUKE_RENDERER_EXCEPTION("vkGetPhysicalDeviceSurfacePresentModesKHR failure")
			std::vector<VkPresentModeKHR> presentModesList(presentModeCount);
			if (VK.GetPhysicalDeviceSurfacePresentModesKHR(this->_vkgpu, this->_vksurface, &presentModeCount, presentModesList.data()) != VK_SUCCESS)
				NUKE_RENDERER_EXCEPTION("vkGetPhysicalDeviceSurfacePresentModesKHR failure")
				
			::VkExtent2D swapchainExtent = {};
			// width and height are either both -1, or both not -1.
			if (vksck.currentExtent.width == (uint32_t)-1) {
				// If the surface size is undefined, the size is set to
				// the size of the images requested.
				swapchainExtent.width = WINDOW_WIDTH;
				swapchainExtent.height = WINDOW_HEIGHT;
			} else {
				// If the surface size is defined, the swap chain size must match
				swapchainExtent = vksck.currentExtent;
			}
			
			// If mailbox mode is available, use it, as is the lowest-latency non-
			// tearing mode.  If not, try IMMEDIATE which will usually be available,
			// and is fastest (though it tears).  If not, fall back to FIFO which is
			// always available.
			::VkPresentModeKHR swapchainPresentMode = VK_PRESENT_MODE_FIFO_KHR;
			for (VkPresentModeKHR vkpmk:presentModesList) {
				if (vkpmk == VK_PRESENT_MODE_MAILBOX_KHR) {
					swapchainPresentMode = VK_PRESENT_MODE_MAILBOX_KHR;
					break;
				}
				else if (vkpmk == VK_PRESENT_MODE_IMMEDIATE_KHR)
					swapchainPresentMode = VK_PRESENT_MODE_IMMEDIATE_KHR;
			}
			
			// Determine the number of VkImage's to use in the swap chain (we desire to
			// own only 1 image at a time, besides the images being displayed and
			// queued for display):
			uint32_t desiredNumberOfSwapChainImages = vksck.minImageCount + 1;
			if ((vksck.maxImageCount > 0) && (desiredNumberOfSwapChainImages > vksck.maxImageCount)) {
				// Application must settle for fewer images than desired:
				desiredNumberOfSwapChainImages = vksck.maxImageCount;
			}
			
			::VkSurfaceTransformFlagBitsKHR preTransform;
			if (vksck.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR) {
				preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
			} else {
				preTransform = vksck.currentTransform;
			}
			
			::VkSwapchainCreateInfoKHR vkscik = {};
			vkscik.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
			vkscik.surface = this->_vksurface;
			vkscik.minImageCount = desiredNumberOfSwapChainImages;
			vkscik.imageFormat = this->_vkformat;
			vkscik.imageExtent = swapchainExtent;
			vkscik.preTransform = preTransform;
			vkscik.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
			vkscik.imageArrayLayers = 1;
			vkscik.presentMode = swapchainPresentMode;
			vkscik.oldSwapchain = VK_NULL_HANDLE;
			vkscik.clipped = true;
			vkscik.imageColorSpace = this->_vkcolorSpace;
			vkscik.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
			vkscik.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
			vkscik.queueFamilyIndexCount = 0;
			vkscik.pQueueFamilyIndices = nullptr;
			
			if (VK.CreateSwapchainKHR(this->_vkdevice, &vkscik, nullptr, &this->_vkswapchain) !=  VK_SUCCESS)
				NUKE_RENDERER_EXCEPTION("vkCreateSwapchainKHR failure")
		}
		
		{
			if (VK.GetSwapchainImagesKHR(this->_vkdevice, this->_vkswapchain, &this->_vkswapchainImageCount, nullptr) != VK_SUCCESS)
				NUKE_RENDERER_EXCEPTION("vkGetSwapchainImagesKHR failure")
				
			std::vector<VkImage> swapchainImages(this->_vkswapchainImageCount);
			if (VK.GetSwapchainImagesKHR(this->_vkdevice, this->_vkswapchain, &this->_vkswapchainImageCount, swapchainImages.data()) != VK_SUCCESS)
				NUKE_RENDERER_EXCEPTION("vkGetSwapchainImagesKHR failure")
				
			this->_vkswapchainBuffers.resize(this->_vkswapchainImageCount);
			
			for (uint32_t i = 0; i < this->_vkswapchainImageCount; ++i) {
				::VkImageViewCreateInfo colorImageView = {};
				colorImageView.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
				colorImageView.format = this->_vkformat;
				colorImageView.components.r = VK_COMPONENT_SWIZZLE_R;
				colorImageView.components.g = VK_COMPONENT_SWIZZLE_G;
				colorImageView.components.b = VK_COMPONENT_SWIZZLE_B;
				colorImageView.components.a = VK_COMPONENT_SWIZZLE_A;
				colorImageView.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				colorImageView.subresourceRange.baseMipLevel = 0;
				colorImageView.subresourceRange.levelCount = 1;
				colorImageView.subresourceRange.baseArrayLayer = 0;
				colorImageView.subresourceRange.layerCount = 1;
				colorImageView.viewType = VK_IMAGE_VIEW_TYPE_2D;
				
				this->_vkswapchainBuffers[i].image = swapchainImages[i];
				
				this->_setImageLayout(this->_vkswapchainBuffers[i].image, 
					VK_IMAGE_ASPECT_COLOR_BIT, 
					VK_IMAGE_LAYOUT_UNDEFINED, 
					VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
					
				colorImageView.image = this->_vkswapchainBuffers[i].image;
				
				if (VK.CreateImageView(this->_vkdevice, &colorImageView, nullptr, &this->_vkswapchainBuffers[i].view) != VK_SUCCESS)
					NUKE_RENDERER_EXCEPTION("vkCreateImageView failure")
			}
		}
	}
	
	virtual void _deinitSwapchain() {
		if (this->_vkswapchainBuffers.size() && VK.DestroyImageView) {
			for (VkSwapChainBuffer vkwcb:this->_vkswapchainBuffers) {
				VK.DestroyImageView(this->_vkdevice, vkwcb.view, nullptr);
			}
		}
		if (this->_vkswapchain && VK.DestroySwapchainKHR)
			VK.DestroySwapchainKHR(this->_vkdevice, this->_vkswapchain, nullptr);
	}
	
	virtual void _initDepthBuffer() {
		if (!(VK.GetPhysicalDeviceFormatProperties = (PFN_vkGetPhysicalDeviceFormatProperties)::glfwGetInstanceProcAddress(this->_vkinstance,"vkGetPhysicalDeviceFormatProperties")))
			NUKE_RENDERER_EXCEPTION("No address for vkGetPhysicalDeviceFormatProperties")
		if (!(VK.CreateImage = (PFN_vkCreateImage)::glfwGetInstanceProcAddress(this->_vkinstance,"vkCreateImage")))
			NUKE_RENDERER_EXCEPTION("No address for vkCreateImage")
		if (!(VK.DestroyImage = (PFN_vkDestroyImage)::glfwGetInstanceProcAddress(this->_vkinstance,"vkDestroyImage")))
			NUKE_RENDERER_EXCEPTION("No address for vkDestroyImage")
		if (!(VK.GetImageMemoryRequirements = (PFN_vkGetImageMemoryRequirements)::glfwGetInstanceProcAddress(this->_vkinstance,"vkGetImageMemoryRequirements")))
			NUKE_RENDERER_EXCEPTION("No address for vkGetImageMemoryRequirements")
		if (!(VK.AllocateMemory = (PFN_vkAllocateMemory)::glfwGetInstanceProcAddress(this->_vkinstance,"vkAllocateMemory")))
			NUKE_RENDERER_EXCEPTION("No address for vkAllocateMemory")
		if (!(VK.FreeMemory = (PFN_vkFreeMemory)::glfwGetInstanceProcAddress(this->_vkinstance,"vkFreeMemory")))
			NUKE_RENDERER_EXCEPTION("No address for vkFreeMemory")
		if (!(VK.BindImageMemory = (PFN_vkBindImageMemory)::glfwGetInstanceProcAddress(this->_vkinstance,"vkBindImageMemory")))
			NUKE_RENDERER_EXCEPTION("No address for vkBindImageMemory")
			
		
	}
	
	virtual void _deinitDepthBuffer() {
	}
	
	GLFW _glfw;
	std::vector<R_VkLayerProperties> _vklayerProperties;
	std::vector<const char*> _vkinstanceExtensionNames;
	std::vector<const char*> _vkinstanceLayerNames;
	std::vector<const char*> _vkdeviceExtensionNames;
	std::vector<const char*> _vkdeviceLayerNames;
	::VkInstance _vkinstance = nullptr;
	::VkPhysicalDevice _vkgpu = nullptr;
	::VkDevice _vkdevice = nullptr;
	uint32_t _vkgraphicsFamilyIndex = 0;
	::GLFWwindow * _glfwWindow = nullptr;
	::VkSurfaceKHR _vksurface = nullptr;
	::VkCommandPool _vkcommandPool = nullptr;
	::VkCommandBuffer _vkcommandBuffer = nullptr;
	::VkFormat _vkformat = VK_FORMAT_UNDEFINED;
	::VkColorSpaceKHR _vkcolorSpace = VK_COLORSPACE_SRGB_NONLINEAR_KHR;
	::VkSwapchainKHR _vkswapchain = nullptr;
	uint32_t _vkswapchainImageCount = 0;
	std::vector<VkSwapChainBuffer> _vkswapchainBuffers;
	::VkQueue _vkqueue = nullptr;
};
}
/* NUKE Renderer END */


int main () {
	el::Loggers::addFlag(el::LoggingFlag::NewLineForContainer);
	el::Loggers::addFlag(el::LoggingFlag::LogDetailedCrashReason);
	el::Loggers::addFlag(el::LoggingFlag::ColoredTerminalOutput);
	el::Loggers::addFlag(el::LoggingFlag::CreateLoggerAutomatically);
	nuke::Renderer renderer;
	renderer.mainLoop();
	
	return 0;
}

