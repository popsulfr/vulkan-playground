# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/popsulfr/work/vulkan-playground/glfw/deps/glad.c" "/home/popsulfr/work/vulkan-playground/glfw-build/tests/CMakeFiles/cursor.dir/__/deps/glad.c.o"
  "/home/popsulfr/work/vulkan-playground/glfw/tests/cursor.c" "/home/popsulfr/work/vulkan-playground/glfw-build/tests/CMakeFiles/cursor.dir/cursor.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "GLFW_DLL"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/popsulfr/work/vulkan-playground/glfw/deps"
  "/home/popsulfr/work/vulkan-playground/glfw/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/popsulfr/work/vulkan-playground/glfw-build/src/CMakeFiles/glfw.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
