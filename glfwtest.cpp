#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <iostream>

class GLFW {
public:
	GLFW() {
		glfwInit();
	}
	
	virtual ~GLFW() {
		glfwTerminate();
	}
};

int main () {
	::glfwSetErrorCallback([](int error,const char* message) {
		std::cerr << "ERROR " << error << " : " << message << std::endl;
	});
	std::cout << ::glfwGetVersionString() << std::endl;
	GLFW();
	std::cout << "Vulkan : " << ::glfwVulkanSupported() << std::endl;
	return 0;
}
