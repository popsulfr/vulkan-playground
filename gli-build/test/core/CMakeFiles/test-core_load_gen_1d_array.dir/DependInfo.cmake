# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/popsulfr/work/vulkan-playground/gli/test/core/core_load_gen_1d_array.cpp" "/home/popsulfr/work/vulkan-playground/gli-build/test/core/CMakeFiles/test-core_load_gen_1d_array.dir/core_load_gen_1d_array.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "SOURCE_DIR=\"/home/popsulfr/work/vulkan-playground/gli\""
  "_CRT_SECURE_NO_WARNINGS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/popsulfr/work/vulkan-playground/gli/."
  "/home/popsulfr/work/vulkan-playground/gli/external/glm"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
